package main

import (
	"encoding/json"
	"fmt"
	"github.com/marni/goigc"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"html/template"
)

func handlerTrack(w http.ResponseWriter, r *http.Request)  {
	switch r.Method {
	case "POST":
		http.Header.Add(w.Header(), "content-type", "application/json")
		parts := strings.Split(r.URL.Path, "/")

		// håndterer /paragliding/api + error
		if len(parts) != 4 || parts[1] != "paragliding"  || parts[2] != "api" {
			http.Error(w, "Malformed URL - in post", http.StatusBadRequest)
			return
		}

		if parts[3] == "track" && len(parts) == 4   {
			// håndterer html template + error
			temp, err := template.ParseFiles("html/igc.gtpl")
			if err != nil {
				http.Error(w, "Error processing template", http.StatusInternalServerError)
			}
			temp.Execute(w, nil)

			htcheck := "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc"
			//htcheck := r.PostFormValue("checkht")
			pattern := `^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[\w]+([\-\.]{1}[\w]+)*\.[a-z]{2,5}(\/.*)?\.igc$`
			res, err := regexp.MatchString(pattern, htcheck)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			if res {
				s := htcheck
				htrack, err := igc.ParseLocation(s)
				if err != nil {
					/*fmt.Errorf("Problem reading the track", err)*/
					http.Error(w, "Problem reading the track", http.StatusInternalServerError)
				}

				intC := ts.CountTracks()
				countP := intC + 1
				convS := strconv.Itoa(countP)
				id := "id" + convS

				var newT Track
				newT.H_date = htrack.Date.String()
				newT.Pilot = htrack.Pilot
				newT.Glider = htrack.GliderType
				newT.Glider_id = htrack.GliderID
				newT.TrackId = string(id)

				ts.AddTrack(newT)

				json.NewEncoder(w).Encode(newT.TrackId)
				return
			} else {
				fmt.Fprintln(w, "Invalid URL")
			}
		} else {
			http.Error(w, "Malformed URL - in post 2", http.StatusBadRequest)
			return
		}
	case "GET": //skal være /api, /api/track, /api/track/<id> eller /api/track/<id>/<field>
		http.Header.Add(w.Header(), "content-type", "application/json")
		parts := strings.Split(r.URL.Path, "/")

		// håndtere /paragliding og error
		if len(parts) < 3 || parts[1] != "paragliding" {
			http.Error(w, "Malformed URL", http.StatusBadRequest)
			return
		}
		// håndtere /paragliding/ error og /api + /api/ error
		if parts[2] == "api" && len(parts) == 3 {
			//se en av forelesningene 28/9 gikk igjennom duration der
			metainfo := MetaInfo{"uptime", "Service for IGC tracks", "v1"}
			json.NewEncoder(w).Encode(metainfo)
		} else if parts[2] != "api" || parts[3] == "" && len(parts) != 3 {
			http.Error(w, "Malformed URL - else if", http.StatusBadRequest)
			return
		}
		// håndterer /track, /track/, /track/<id>,  og error
		if parts[3] == "track" && len(parts) == 4 {
			if /*ts.tracks == nil*/ ts.CountTracks() == 0 { // hvis in-mem er tomt vises tom array
				json.NewEncoder(w).Encode([]Track{})
			} else {
				a := make([]Track, 0, /*len(ts.tracks)*/ ts.CountTracks())
				for _, t := range /*ts.tracks*/ ts.GetAllTracks() {
					a = append(a, t)
				}
				json.NewEncoder(w).Encode(a)
			}
		} else if parts[3] != "track" || parts[4] == "" && len(parts) != 4 {
			http.Error(w, "Malformed URL - else if len 4", http.StatusBadRequest)
			return
		} else if parts[4] != "" && len(parts) == 5 {
			pattern := `^id[0-9]+$`
			tId := parts[4]
			res, _ := regexp.MatchString(pattern, tId)  // sjekke om id-string er ihht satt pattern for syst.
			if res {
				t, ok := ts.GetTrack(tId) // sjekke om id'en er registrert
				if !ok {
					http.Error(w, "Id not registered", http.StatusNotFound)
					return
				}
				json.NewEncoder(w).Encode(t)
			} else {
				http.Error(w, "Wrong id, did not match pattern", http.StatusInternalServerError)
				return
			}
		} else if parts[5] == "" && len(parts) == 6 {
			pattern := `^id[0-9]+$`
			tId := parts[4]
			res, _ := regexp.MatchString(pattern, tId)  // sjekke om id-string er ihht satt pattern for syst.
			if res {
				t, ok := ts.GetTrack(tId) // sjekke om id'en er registrert
				if !ok {
					http.Error(w, "Id not registered - inside plain text", http.StatusNotFound)
					return
				}
				fmt.Fprintf(w, "Not implemented yet for track: %v", t.TrackId)
			} else {
				http.Error(w, "Wrong id, did not match pattern", http.StatusInternalServerError)
				return
			}
		}
	default:
		http.Error(w, "Not implemented yet", http.StatusNotImplemented)
		return
	}
}


