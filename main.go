package main

import (
	"google.golang.org/appengine"
	"net/http"
)

//global variabel
var ts TracksMongoDB

func main() {
	ts = TracksMongoDB{
		DatabaseURL: "mongodb://inger:inger123@ds261521.mlab.com:61521/imtracks",
		DatabaseName: "imtracks",
		TracksCollectionName: "Tracks",
	}  //database lagring
	ts.Init() // initierer ts

	http.HandleFunc("/", handlerTrack)
	appengine.Main() // starter server for mottak av requests
	//http.ListenAndServe(":8080", nil)
}
