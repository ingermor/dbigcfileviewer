package main

import (
	"github.com/globalsign/mgo"
	"testing"
)

// intiterer databasen
func setupDB(t *testing.T) *TracksMongoDB {
	ts := TracksMongoDB{
		DatabaseURL: "mongodb://localhost",
		DatabaseName: "testTracksDB",
		TracksCollectionName: "Tracks",
	}
	session, err := mgo.Dial(ts.DatabaseURL)
	defer  session.Close()

	if err != nil {
		t.Error(err)
	}
	return &ts
}

//avslutte databasen
func tearDownDB(t *testing.T, ts *TracksMongoDB)  {
	session, err := mgo.Dial(ts.DatabaseURL)
	defer  session.Close()

	if err != nil {
		t.Error(err)
	}

	err = session.DB(ts.DatabaseName).DropDatabase()
	if err != nil {
		t.Error(err)
	}
}

func TestTracksMongoDB_AddTrack(t *testing.T) {
	ts := setupDB(t)
	defer tearDownDB(t, ts)

	ts.Init()
	if ts.CountTracks() != 0 {
		t.Error("database not properly initialized, countTracks() should be 0.")
	}

	track := Track{"271018", "TestPilot", "Yalla", "Yalla234", "id1"}
	ts.AddTrack(track)

	if ts.CountTracks() != 1 {
		t.Error("adding new track failed.")
	}
}

func TestTracksMongoDB_GetTrack(t *testing.T) {
	ts := setupDB(t)
	defer tearDownDB(t, ts)

	ts.Init()
	if ts.CountTracks() != 0 {
		t.Error("database not properly initialized, countTracks() should be 0.")
	}

	track := Track{"271018", "TestPilot", "Yalla", "Yalla234", "id1"}
	ts.AddTrack(track)

	if ts.CountTracks() != 1 {
		t.Error("adding new track failed.")
	}

	newTrack, ok := ts.GetTrack(track.TrackId)
	if !ok {
		t.Error("could not find the track")
	}
	if newTrack.H_date != track.H_date ||
		newTrack.Pilot != track.Pilot ||
		newTrack.Glider != track.Glider ||
		newTrack.Glider_id != track.Glider_id ||
		newTrack.TrackId != track.TrackId {
		t.Error("tracks do not match")
	}

	all := ts.GetAllTracks()
	if len(all) !=1 || all[0].TrackId != track.TrackId {
		t.Error("GetAllTracks() doesen't return proper slice of all the items")
	}
}

func TestTracksMongoDB_Duplicates(t *testing.T) {
	ts := setupDB(t)
	defer tearDownDB(t, ts)

	ts.Init()
	if ts.CountTracks() != 0 {
		t.Error("database not properly initialized, countTracks() should be 0.")
	}

	track := Track{"271018", "TestPilot", "Yalla", "Yalla234", "id1"}
	err := ts.AddTrack(track)
	if err != nil {
		t.Error(err)
	}

	if ts.CountTracks() != 1 {
		t.Error("adding new track failed.")
	}

	err = ts.AddTrack(track)
	if err == nil {
		t.Error("adding duplicate entry should generate an error, but it doesen't")
	}

	if ts.CountTracks() != 1 {
		t.Error("adding new track failed.")
	}
}
