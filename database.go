package main

import (
	"fmt"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

// Lagrer detaljene for databasetilkoblingen
type TracksMongoDB struct {
	DatabaseURL string
	DatabaseName string
	TracksCollectionName string
}

// info om api
type MetaInfo struct {
	Uptime  string `json:"uptime"`
	Info    string `json:""`
	Version string `json:"version"`
}

// innhold i track datalagringssstruktur JSON
type Track struct {
	H_date    string `json:"h_date"`
	Pilot     string `json:"pilot"`
	Glider    string `json:"glider"`
	Glider_id string `json:"glider_id"`
	//Track_length float32 `json:"track_length"`
	TrackId string `json:"trackid"`  //endret for å kunne bruke database
}

// funksjon for å intitiere lagring i mongo databasen
func (ts *TracksMongoDB) Init() {
	// lager/aksesserer database
	session, err := mgo.Dial(ts.DatabaseURL)
	if err != nil {  // hvis ingen kontakt med DB
		panic(err)	 // "exit"
	}
	// stenger oppkoblingen mot databasen etter endt utførelse av funksjonen
	defer session.Close()

	// legg til ekstra begrensninger på track collection ifht trackid
	// benytter indeks til dette
	index := mgo.Index{
		Key: []string{"trackid"},  // array av strings type trackid
		Unique: true,   // skal være unik
		DropDups: true,   // "slipp" doc med samme indeks nøkkel som en tidligere nøkkel
		Background: true,    // bygg indeks i bakgrun og returnerer øyeblikkelig
		Sparse: true,   // sett indeks kun på doc med nøkkelfelt
	}

	err = session.DB(ts.DatabaseName).C(ts.TracksCollectionName).EnsureIndex(index)
	if err != nil {
		panic(err)
	}
}

// funksjon for å legge til en ny track i mongo databasen
func (ts *TracksMongoDB) AddTrack(t Track) error {
	// lager/aksesserer database
	session, err := mgo.Dial(ts.DatabaseURL)
	if err != nil {  // hvis ingen kontakt med DB
		panic(err)	 // "exit"
	}
	// stenger oppkoblingen mot databasen etter endt utførelse av funksjonen
	defer session.Close()

	//åpner opp for håndtering av database -> collection og legger til ny track
	err = session.DB(ts.DatabaseName).C(ts.TracksCollectionName).Insert(t)
	if err != nil {
		fmt.Printf("error in Insert(): %v", err.Error())
		return err
	}
	return nil
}

// funksjon for å telle antall tracks i mongo databasen
// returnerer antall
func (ts *TracksMongoDB) CountTracks() int {
	// lager/aksesserer database
	session, err := mgo.Dial(ts.DatabaseURL)
	if err != nil {  // hvis ingen kontakt med DB
		panic(err)	 // "exit"
	}
	// stenger oppkoblingen mot databasen etter endt utførelse av funksjonen
	defer session.Close()

	//åpner opp for håndtering av database -> collection og utfører telling av den
	count, err := session.DB(ts.DatabaseName).C(ts.TracksCollectionName).Count()
	if err != nil {
		fmt.Printf("error in Count(): %v", err.Error())
		return -1
	}
	return count
}

// funksjon for å hente track på bakgrun av keyId
// returnerer track keyID og bool ok (hvis ok)
func (ts *TracksMongoDB) GetTrack(keyId string) (Track, bool) {
	// lager/aksesserer database
	session, err := mgo.Dial(ts.DatabaseURL)
	if err != nil {  // hvis ingen kontakt med DB
		panic(err)	 // "exit"
	}
	// stenger oppkoblingen mot databasen etter endt utførelse av funksjonen
	defer session.Close()

	//åpner opp for håndtering av database -> collection og utfører søk etter track
	track := Track{}  //empty track
	allWasGood := true  // bool
	err = session.DB(ts.DatabaseName).C(ts.TracksCollectionName).Find(bson.M{"trackid": keyId}).One(&track)
	if err != nil {
		allWasGood = false
	}
	return track, allWasGood
}

// funksjon for å hente alle tracks
func (ts *TracksMongoDB) GetAllTracks() []Track {
	// lager/aksesserer database
	session, err := mgo.Dial(ts.DatabaseURL)
	if err != nil { // hvis ingen kontakt med DB
		panic(err) // "exit"
	}
	// stenger oppkoblingen mot databasen etter endt utførelse av funksjonen
	defer session.Close()

	var all []Track
	err = session.DB(ts.DatabaseName).C(ts.TracksCollectionName).Find(bson.M{}).All(&all)
	if err != nil {
		return []Track{}
	}
	return all
}
